import {calculatePlayResult} from './rock-paper-scissors.utils';
import {GameResult, RockPaperScissors} from './rock-paper-scissors.definitions';

describe('Rock Paper Scissors', () => {

  describe('Utils', () => {
    // Test all code paths, not all code combinations
    const testData = [
      {
        player1move: RockPaperScissors.PAPER,
        player2move: RockPaperScissors.ROCK,
        result: GameResult.WIN
      },
      {
        player1move: RockPaperScissors.ROCK,
        player2move: RockPaperScissors.SCISSORS,
        result: GameResult.WIN
      },
      {
        player1move: RockPaperScissors.SCISSORS,
        player2move: RockPaperScissors.PAPER,
        result: GameResult.WIN
      },
      {
        player1move: RockPaperScissors.ROCK,
        player2move: RockPaperScissors.PAPER,
        result: GameResult.LOSE
      },
      {
        player1move: RockPaperScissors.SCISSORS,
        player2move: RockPaperScissors.ROCK,
        result: GameResult.LOSE
      },
      {
        player1move: RockPaperScissors.PAPER,
        player2move: RockPaperScissors.SCISSORS,
        result: GameResult.LOSE
      },
      {
        player1move: RockPaperScissors.ROCK,
        player2move: RockPaperScissors.ROCK,
        result: GameResult.DRAW
      },
      {
        player1move: null,
        player2move: RockPaperScissors.ROCK,
        result: null
      }
    ];

    it('should return the right game result', () => {
      testData.forEach((data) => {
        expect(calculatePlayResult(data.player1move, data.player2move)).toBe(data.result);
      });

    });
  });


});
