import {Component, OnInit} from '@angular/core';
import {GameResult, RockPaperScissors} from './rock-paper-scissors.definitions';
import {calculatePlayResult, computerCalculatePlay} from './rock-paper-scissors.utils';

@Component({
  selector: 'app-rock-paper-scissors',
  templateUrl: 'rock-paper-scissors.component.html',
  styleUrls: ['./rock-paper-scissors.component.scss']
})

export class RockPaperScissorsComponent implements OnInit {
  wins = 0;
  loses = 0;
  draws = 0;
  userPlayerValue: RockPaperScissors;
  computerPlayerValue: RockPaperScissors;

  constructor() {
  }

  ngOnInit() {
  }

  setUserPlayerValue(val: RockPaperScissors) {
    this.userPlayerValue = val;
  }

  play() {
    // calculate computer move
    this.computerPlayerValue = computerCalculatePlay();
    // check game winner
    const gameResult: GameResult = calculatePlayResult(this.userPlayerValue, this.computerPlayerValue);
    if (gameResult === GameResult.WIN) {
      this.wins++;
    } else if (gameResult === GameResult.LOSE) {
      this.loses++;
    } else if (gameResult === GameResult.DRAW) {
      this.draws++;
    }
  }
}
