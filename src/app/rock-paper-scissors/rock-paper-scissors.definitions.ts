export enum RockPaperScissors {
  ROCK = 'rock',
  PAPER = 'paper',
  SCISSORS = 'scissors'
}

export enum GameResult {
  WIN = 'win',
  LOSE = 'lose',
  DRAW = 'draw'
}
