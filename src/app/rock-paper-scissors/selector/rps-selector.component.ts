import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RockPaperScissors} from '../rock-paper-scissors.definitions';

@Component({
  selector: 'app-rps-selector',
  templateUrl: 'rps-selector.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class RpsSelectorComponent implements OnInit {
  @Input() placeholder: string;
  @Input() disabled = false;
  @Input() value?: RockPaperScissors;
  @Output() itemSelected: EventEmitter<RockPaperScissors> = new EventEmitter();

  selectData = Object.values(RockPaperScissors);
  constructor() {

  }

  ngOnInit() {
  }

  onValueChange(val) {
    this.itemSelected.next(val);
  }
}
