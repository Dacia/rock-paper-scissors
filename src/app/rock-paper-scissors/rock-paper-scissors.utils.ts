import {GameResult, RockPaperScissors} from './rock-paper-scissors.definitions';

export function computerCalculatePlay(): any {
  return Object.values(RockPaperScissors)[Math.floor(Math.random() * 3)];
}

/**
 * Returns the GameResult relative to the playerMove1
 * @param playerMove1
 * @param playerMove2
 */
export function calculatePlayResult(playerMove1: RockPaperScissors, playerMove2: RockPaperScissors): GameResult {
  if (playerMove1 === playerMove2) {
    return GameResult.DRAW;
  }
  if (playerMove1 === RockPaperScissors.PAPER) {
    return playerMove2 === RockPaperScissors.ROCK ? GameResult.WIN : GameResult.LOSE;
  } else if (playerMove1 === RockPaperScissors.ROCK) {
    return playerMove2 === RockPaperScissors.SCISSORS ? GameResult.WIN : GameResult.LOSE;
  } else if (playerMove1 === RockPaperScissors.SCISSORS) {
    return playerMove2 === RockPaperScissors.PAPER ? GameResult.WIN : GameResult.LOSE;
  } else {
    // if non of the others just return null
    return null;
  }
}
