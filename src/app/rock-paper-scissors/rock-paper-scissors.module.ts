import {NgModule} from '@angular/core';
import {RockPaperScissorsComponent} from './rock-paper-scissors.component';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatSelectModule} from '@angular/material';
import {RpsSelectorComponent} from "./selector/rps-selector.component";

@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatButtonModule
  ],
  exports: [
    RockPaperScissorsComponent,
    RpsSelectorComponent
  ],
  declarations: [
    RockPaperScissorsComponent,
    RpsSelectorComponent
  ],
  providers: [],
})
export class RockPaperScissorsModule {
}
