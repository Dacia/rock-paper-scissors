import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RockPaperScissorsModule } from './rock-paper-scissors/rock-paper-scissors.module';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RockPaperScissorsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
